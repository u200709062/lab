public class MyDate {
    int day;
    int month;
    int year;

    int[] maxDays = {31,29,31,30,31,30,31,31,30,31,30,31};

    public MyDate(int day, int month, int year){
        this.day = day;
        this.month = month;
        this.year = year;
    }
    public String toString(){
        return year + "-" + (month < 10 ? "0" : "" ) + month + "-" + (day<10 ? "0" : "") + day;
    }
    public void incrementDay(){
        day++;
        if (day>maxDays[month-1]){
            day = 1;
            month++;
        }
        else if (month == 2 && day == 29 && !inLeapYear()){
            day = 1;
            month++;
        }
    }

    public boolean inLeapYear() {
        return year % 4 == 0;
    }

    public void incrementYear(int diff){
        year = year + diff;
        if (month == 2 && day == 29 && !inLeapYear()){
            day = 28;
        }
    }

    public void decrementDay(){
        day--;
        if (day == 0){
            decrementMonth();
            if (!inLeapYear() && month == 2)
                day = 28;
            else
                day = maxDays[month-1];
        }
    }
    public void decrementYear(){
        incrementYear(-1);
    }

    public void decrementMonth(){
        decrementMonth(1);
    }
    public void incrementDay(int diff){
        while (diff > 0 ){
            incrementDay();
            diff--;
        }
    }
    public void decrementMonth(int diff){
        incrementMonth(-diff);
    }
    public void decrementDay(int diff){ //issue is here!!! solved for now.
        day--;
        while (diff > 0 ) {
            decrementDay();
            diff--;
        }
        day = maxDays[month-1];
    }

    public void incrementMonth(int diff){ //check
        month += diff;
        int yearDiff = (month-1) / 12;
        int newMonth = ((month-1) % 12) + 1;

        if (newMonth < 0)
            yearDiff--;

        year += yearDiff;
        month = newMonth < 0 ? newMonth + 12 : newMonth;

        if (day > maxDays[month-1]){
            day = maxDays[month-1];
            if (!inLeapYear() && month == 2)
                day = 28;
        }
    }

    public void decrementYear(int diff){
        incrementYear(-diff);
    }
    public void incrementMonth(){
        incrementMonth(1);
    }
    public void incrementYear(){
        incrementYear(1);
    }

    public boolean isBefore(MyDate anotherDate) {
        if (year > anotherDate.year) {
            return false;
        }else if (year == anotherDate.year && month > anotherDate.month) {
            return false;
        }else if (year == anotherDate.year && month == anotherDate.month && day > anotherDate.day) {
            return false;
        }
        return true;
    }
    public boolean isAfter(MyDate anotherDate) {
        if (year > anotherDate.year) {
            return true;
        }else if (year == anotherDate.year && month > anotherDate.month) {
            return true;
        }else if (year == anotherDate.year && month == anotherDate.month && day > anotherDate.day) {
            return true;
        }
        return false;
    }
    public int dayDifference(MyDate anotherDate) {
        int count = 0;
        while(isBefore(anotherDate)==true){
            anotherDate.incrementDay();
            count++;
            if(anotherDate.year == year && anotherDate.month == month && anotherDate.day == day){
                break;
            }
        }
        while(isAfter(anotherDate)==true){
            anotherDate.incrementDay();
            count++;
            if(anotherDate.year == year && anotherDate.month == month && anotherDate.day == day){
                break;
            }        
    }
        return count;
 }
}

