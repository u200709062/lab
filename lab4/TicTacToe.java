import java.io.IOException;
import java.util.Scanner;

public class TicTacToe {

	public static void main(String[] args) throws IOException {
		Scanner reader = new Scanner(System.in);
		char[][] board = { { ' ', ' ', ' ' }, { ' ', ' ', ' ' }, { ' ', ' ', ' ' } };

		int moveCount = 0;
		int currentPlayer = 0;
		printBoard(board);

		while (moveCount<9) {
			System.out.print("Player " + (currentPlayer + 1) + " enter row number:");
			int row = reader.nextInt();
			System.out.print("Player "+(currentPlayer + 1)+" enter column number:");
			int col = reader.nextInt();

			if (((row>0 && row<4) && (col>0 && col<4)) && (board[row-1][col-1] == ' ')) {
				board[row - 1][col - 1] = currentPlayer == 0 ? 'X' : 'O'; //short if else statement
				moveCount++;
				printBoard(board);

				boolean win = checkboard(board , row-1 , col-1);
				if (win){
					System.out.println("Player "+ (currentPlayer+1) +" is the winner.");
					break;
				}
				currentPlayer = (currentPlayer + 1) % 2;
			}
			else {
				System.out.println("\n It is not a valid move.\n");
			}
		}
		reader.close();
	}

	private static boolean checkboard(char[][] board, int row, int col) {
		char symbol = board[row][col];
		boolean win = true;
		for (int c = 0; c < 3; c++) {
			if (board[row][c] != symbol) {
				win = false;
				break;
			}
		}
		if (win)
			return true;

		win = true;
		for (int r = 0; r < 3; r++) {
			if (board[r][col] != symbol) {
				win = false;
				break;
			}
		}
		if (win)
			return true;

		if (row==col){
			win =true;
			for (int i = 0; i<3 ; i++){
				if ((board[i][i])!=symbol){
				win = false ;
				break;
				}
			}
		}
		if (win)
			return true;
		//hw 
		win = true;
		for (int k =0; k <3 ;k++){
		    for (int l =0; l < 3; l++){
		        if ((k - l)==2 || (k - l) == -2 ){
		            if (board[k][l] != symbol){
		            win = false;
		            break;
		            }
		        }
		        else if ( k ==1 && l ==1){
		            if (board[k][l] != symbol){
		            win = false;
		            break;
		            }
		        }
		        
		    }
		}
		return win;
	}

	public static void printBoard(char[][] board) {
		System.out.println("    1   2   3");
		System.out.println("   -----------");
		for (int row = 0; row < 3; ++row) {
			System.out.print(row + 1 + " ");
			for (int col = 0; col < 3; ++col) {
				System.out.print("|");
				System.out.print(" " + board[row][col] + " ");
				if (col == 2)
					System.out.print("|");

			}
			System.out.println();
			System.out.println("   -----------");

		}

	}

}