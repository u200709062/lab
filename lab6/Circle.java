public class Circle {
    //we can also use static final double PI = 3.14 or Math.PI
    static final double PI = 3.14;
    int radius;
    Point center;

    public Circle(int radius, Point center) {
        this.radius = radius;
        this.center = center;
    }
    public double area() {
        return PI * radius * radius;
    }
    public double perimeter() {
        return 2 * PI * radius;
    }
    public boolean intersect(Circle circle) {
        boolean intersect = false;
        
         return radius + circle.radius >= center.distancefromPoint(circle.center);

    }
}
