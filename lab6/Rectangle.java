public class Rectangle {

    private Point topLeft;
    private int widht;
    private int height;

    public Rectangle( Point topLeft, int widht, int height ) {
        this.topLeft = topLeft;
        this.widht   = widht;
        this.height  = height;
    }
    
    public int area() {
        return widht * height;
    }
    public int parimeter(){
        return 2 * ( widht + height);
    }
    public Point[] cornes() {
        Point[] cornes = new Point[4];
        cornes[0] = topLeft;
        cornes[1] = new Point(topLeft.getxCoord() + widht, topLeft.getyCoord());
        cornes[2] = new Point(topLeft.getxCoord(), topLeft.getyCoord() - height);
        cornes[3] = new Point(topLeft.getxCoord() + widht, topLeft.getyCoord() - height);

        return cornes;
    }
}
